<?php

/**
 * Theme Setup functions
 * 
 * @package Affiliate Revolution
 */

if (!function_exists('ar_setup')) :
    add_action('after_setup_theme', 'ar_setup');
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     */
    function ar_setup()
    {

        /**
         * Make theme available for translation.
         * Translations can be placed in the /languages/ directory.
         */
        load_theme_textdomain('affiliate-revolution', get_template_directory() . '/languages');

        /**
         * Add theme support for various features.
         */
        add_theme_support('post-thumbnails');
        add_theme_support('custom-logo');
        add_theme_support('title-tag');
        add_theme_support('post-formats', array('quote', 'image', 'video'));
        add_theme_support('html5', array('search-form', 'caption', 'script', 'style'));
        add_theme_support('responsive-embeds');

        /**
         * Add support for two custom navigation menus.
         */
        register_nav_menus(array(
            'main-menu'   => 'Main Menu',
            'mobile-menu' => 'Mobile Menu',
            'footer-menu-1' => 'Footer Menu 1',
            'footer-menu-2' => 'Footer Menu 2',
            'footer-menu-3' => 'Footer Menu 3',
            'footer-menu-4' => 'Footer Menu 4',
        ));

        register_sidebar(array(
            'name'          => __('Sidebar - 1', 'casinon'),
            'id'            => 'sidebar-1',
            'description'   => __('Custom sidebar', 'casinon'),
        ));

        register_sidebar(array(
            'name'          => __('Sidebar - 2', 'casinon'),
            'id'            => 'sidebar-2',
            'description'   => __('Custom sidebar', 'casinon'),
        ));
    }
endif;

/**
 * Remove the content editor from ALL pages 
 */
function remove_content_editor()
{
    remove_post_type_support('page', 'editor');
}
add_action('admin_head', 'remove_content_editor');
