<?php

/**
 * Affiliate Product custom post type 
 * 
 * @package Affiliate Revolution
 */

function ar_product_init()
{
    $labels = array(
        'name'                  => _x('Affiliate Product', 'Post type general name', 'affiliate-revolution'),
        'singular_name'         => _x('Affiliate Product', 'Post type singular name', 'affiliate-revolution'),
        'menu_name'             => _x('Affiliate Product', 'Admin Menu text', 'affiliate-revolution'),
        'name_admin_bar'        => _x('Affiliate Product', 'Add New on Toolbar', 'affiliate-revolution'),
    );

    $args = array(
        'labels' => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array('slug' => 'product'),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-share',
        'supports'           => array('title', 'author', 'thumbnail', 'comments'),
    );

    register_post_type('product', $args);
}

add_action('init', 'ar_product_init');

/**
 * Register casino taxonomy - category 
 */
function ar_product_taxonomy()
{
    $args = array(
        'label'        => __('Category', 'casinon'),
        'public'       => false,
        'show_ui'   => true,
        'rewrite'      => false,
        'hierarchical' => true
    );

    register_taxonomy('product-category', 'product', $args);
}
add_action('init', 'ar_product_taxonomy');



//Changes the slug of the custom post type
// function casinon_change_casino_slug($args, $post_type)
// {

//     if ('casino' === $post_type && get_field('casinon_casino_change_slug', 'options')) {
//         $args['rewrite']['slug'] = get_field('casinon_casino_change_slug', 'options');
//     }

//     return $args;
// }

// add_filter('register_post_type_args', 'casinon_change_casino_slug', 10, 2);
