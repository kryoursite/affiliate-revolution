<?php

/**
 * Helper functions.
 *
 * @package Affiliate Revolution
 */

/**
 * Create footer menu
 */
function ar_generate_footer_menu($name)
{
    $args = array(
        'theme_location' => "$name",
        'container' => 'div',
        'echo' => false,
    );

    $menu = "";

    $menu .= "<div class='footer-column'>";

    $menu .= "<span>";

    $menu .= wp_get_nav_menu_name($name);

    $menu .= "</span>";

    $menu .= wp_nav_menu($args);

    $menu .= "</div>";

    return $menu;
}

function ar_print_star_rating($rating)
{
    $number = $rating;
    $average_stars = round($number * 2) / 2;
    $drawn = 5;
    for ($i = 0; $i < floor($average_stars); $i++) {
        $drawn--;
        echo '<i class="fas fa-star"></i>';
    }

    if ($number - floor($average_stars) == 0.5) {
        $drawn--;
        echo '<i class="fas fa-star-half-alt"></i>';
    }

    for ($i = $drawn; $i > 0; $i--) {
        echo '<i class="fas fa-star empty-star"></i>';
    }
}
