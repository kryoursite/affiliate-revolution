<?php

/**
 * General functions.
 *
 * @package Affiliate Revolution
 */

/**
 * Register and Enqueue Styles.
 */



function ar_register_styles()
{
    $css_path = AR_TEMPLATE_DIR . '/dist/css/style.css';

    if (DEVELOPMENT) {
        wp_enqueue_style('casinon-style', AR_DIR_URI . '/dist/css/style.css', array(), filemtime($css_path));
        wp_enqueue_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css');
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap');
        wp_enqueue_style('page-font', AR_DIR_URI . '/dist/css/fonts.css', array(), '1.0.0');
    } else {
        wp_enqueue_style('casinon-style', AR_DIR_URI . '/dist/css/style.css', array(), AR_VERSION);
        wp_enqueue_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css');
        wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap');
    }
}

add_action('wp_enqueue_scripts', 'ar_register_styles');

/**
 * Register and Enqueue Scripts.
 */
function ar_register_scripts()
{
    $js_path = AR_TEMPLATE_DIR . '/dist/js/scripts.js';

    if (DEVELOPMENT) {
        wp_enqueue_script('casinon-scripts', AR_DIR_URI . '/dist/js/scripts.js', array('jquery'), filemtime($js_path), false);
        wp_script_add_data('casinon-scripts', 'async', true);
    } else {
        wp_enqueue_script('casinon-scripts', AR_DIR_URI . '/dist/js/scripts.js', array('jquery'), AR_VERSION, false);
        wp_script_add_data('casinon-scripts', 'async', true);
    }
}

add_action('wp_enqueue_scripts', 'ar_register_scripts');


function ar_register_admin_style()
{
    wp_enqueue_style('admin-style', AR_DIR_URI . '/dist/css/admin-style.css', array(), filemtime($css_path));
}

add_action('admin_enqueue_scripts', 'ar_register_admin_style');
