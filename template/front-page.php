<?php

/**
 * The template for displaying front page
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<main class="site-main front-page" role="main" style="padding-top: 200px;">
    <div class="page-navigation">
        <div class="container">
            <div class="inner-page-navigation">
                <ul class="page-nav-list" data-toc data-toc-headings="h2"></ul>
            </div>
        </div>
    </div>
    <div class="page-content">

        <?php

        // Check value exists.
        if (have_rows('content_blocks')) :

            // Loop through rows.
            while (have_rows('content_blocks')) : the_row();

                // Case: Paragraph layout.
                if (get_row_layout() == 'top_products') :

                    get_template_part('template-parts/content-blocks/product-top');

                elseif (get_row_layout() == 'compare') :

                    get_template_part('template-parts/content-blocks/product-compare');

                elseif (get_row_layout() == 'cards') :

                    get_template_part('template-parts/content-blocks/product-card');

                elseif (get_row_layout() == 'which_one') :

                    get_template_part('template-parts/content-blocks/product-which-one');

                elseif (get_row_layout() == 'promotion') :

                    get_template_part('template-parts/content-blocks/product-promotion');

                elseif (get_row_layout() == 'newsletter') :

                    get_template_part('template-parts/content-blocks/newsletter');

                endif;

            // End loop.
            endwhile;

        // No value.
        else :
        // Do something...
        endif;

        ?>


    </div>
</main>