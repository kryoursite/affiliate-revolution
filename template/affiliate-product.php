<?php

/**
 * The template for displaying front page
 * 
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<main class="site-main" role="main" style="padding-top: 150px;">
    
    <div class="page-content">

        <?php if (is_single()) comments_template('/template/comment.php'); ?>

    </div>
</main>