<?php

/**
 * Header navigation template part
 * 
 */

?>

<header id="site-header" class="site-header" role="banner">
    <div class="header-inner">
        <div class="site-header__logo">
            <?php the_custom_logo(); ?>
        </div>
        <div class="site-header__nav">
            <?php
            $args = array(
                'theme_location' => 'main-menu',
                'container' => 'nav',
                'container_class' => 'main-menu',
            );
            wp_nav_menu($args);
            ?>
        </div>
        <div class="site-header__profile">
            <div class="profile-inner">
                <div class="languages"><i class="fas fa-globe-europe"></i> <span>En</span> <i class="fas fa-angle-down"></i></div>
                <div class="signin">Sign In</div>
                <div class="signup">Signup</div>
            </div>
        </div>
    </div>
</header>