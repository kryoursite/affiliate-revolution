<?php

/**
 * Front page header block
 *
 * @package Casinon
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?>

<div class="front-header-block" style="background-image: url(<?php echo AR_DIR_URI . '/dist/img/desktop-layout.png' ?>);">
    <div class="front-block-inner container">
        <div class="front-block-wrap">
            <h1 class="front-title"><?php the_title(); ?></h1>

            <?php if (get_field('front_intro_text')) : ?>
                <p class="front-intro-text"><?php the_field('front_intro_text'); ?></p>
            <?php endif; ?>
            
            <span class="front-learn-more">Learn more <i class="fas fa-angle-down"></i></span>
        </div>
        <div class="front-categories">
            <?php $terms = get_field('front_categories');

            if ($terms) : ?>
                <?php foreach ($terms as $term) : ?>
                    <?php $image = get_field('product_category_image', 'product-category_' . $term->term_id); ?>
                    <div class="single-category" style="background-image: url(<?php echo $image['url']; ?>);">
                        <span><?php echo esc_html($term->name); ?></span>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

        </div>
    </div>
</div>