<div class="content-block compare-block">
    <div class="compare-title">
        <div class="container">
            <h2><?php echo get_sub_field('compare_title'); ?></h2>
        </div>
    </div>
    <div class="compare-products">
        <?php
        $post_objects = get_sub_field('compare_products_display');
        if ($post_objects) : ?>

            <?php foreach ($post_objects as $post) : ?>
                <?php setup_postdata($post); ?>

            <?php endforeach; ?>

            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
            ?>
        <?php endif; ?>
    </div>

</div>