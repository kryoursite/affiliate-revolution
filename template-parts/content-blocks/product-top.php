<div class="content-block top-products-block">
    <div class="top-products-title">
        <div class="container">
            <h2><?php echo get_sub_field('top_products_title'); ?></h2>
        </div>
    </div>
    <div class="top10-products">
        <?php
        $post_objects = get_sub_field('top_products_display');
        if ($post_objects) : ?>

            <?php foreach ($post_objects as $post) : ?>
                <?php setup_postdata($post); ?>
                <div class="top10-single">
                    <div class="top10-single-inner container">
                        <div class="top10-image">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <div class="top10-options">
                            <div class="options-upper">
                                <div class="top10-title"><?php the_title(); ?></div>
                                <div class="add-to-favorites"><i class="far fa-heart"></i></div>
                                <?php if (get_field('ap_top_website', $post->ID)) : ?>
                                    <div class="top10-site"><?php the_field('ap_top_website', $post->ID); ?></div>
                                <?php endif; ?>
                            </div>
                            <div class="options-lower">

                                <?php if (get_field('ap_rating', $post->ID)) : ?>
                                    <div class="star-rating">
                                        <?php ar_print_star_rating(get_field('ap_rating', $post->ID)); ?>
                                    </div>
                                <?php endif; ?>

                                <div class="user-rating">
                                    <div class="like"><i class="fas fa-heart"></i> 200</div>
                                    <div class="dislike"><i class="fas fa-thumbs-down"></i> 29</div>
                                </div>
                            </div>
                        </div>
                        <?php if (get_field('ap_top_selling_point', $post->ID)) : ?>
                            <div class="top10-selling-point">
                                <?php the_field('ap_top_selling_point', $post->ID); ?>
                            </div>
                        <?php endif; ?>

                        <div class="top10-nav">
                            <a class="go-to-profile" href="#"><?php _e('Go to Profile', 'affiliate-revolution'); ?></a>
                            <a class="read-review" href="#"><?php _e('Read reviews', 'affiliate-revolution'); ?></a>
                        </div>
                        <?php if (get_field('ap_top_enable_learn_more', $post->ID)) : ?>
                            <div class="top10-learn-more">
                                <span><?php _e('Learn More', 'affiliate-revolution'); ?> <i class="fas fa-angle-down"></i></span>
                            </div>
                            <div class="top10-learn-more-block">
                                <?php the_field('ap_top_learn_more_text', $post->ID); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>

            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
            ?>
        <?php endif; ?>
    </div>

</div>