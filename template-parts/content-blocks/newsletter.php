<?php

$form_id = get_sub_field('newsletter_id');

?>

<div class="content-block newsletter">
    <div class="container">
        <div class="newsletter-wrap">
            <div class="newsletter-title">
                Subscribe to our Newsletter
            </div>
            <div class="newsletter-form">
                <?php echo do_shortcode("[mc4wp_form id=$form_id]"); ?>
            </div>
        </div>

    </div>
</div>