<div class="content-block promotion-block">

    <div class="promotion-overlay">

        <div class="container">

            <div class="promotion-content">
                <div class="promo-left">
                    <div class="promo-subtitle">
                        <?php the_sub_field('promo_subtitle'); ?>
                    </div>
                    <div class="promo-title">
                        <?php the_sub_field('promo_title'); ?>
                    </div>
                    <?php if (get_sub_field('promo_rating')) : ?>
                        <div class="star-rating">
                            <?php ar_print_star_rating(get_sub_field('promo_rating')); ?>
                        </div>
                    <?php endif; ?>
                    <div class="promo-text">
                        <?php the_sub_field('promo_text'); ?>
                    </div>
                </div>
                <div class="promo-right">
                    <?php if (get_sub_field('promo_btn_text')) : ?>
                        <div class="promo-button">
                            <a href="/"><?php the_sub_field('promo_btn_text'); ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

        </div>

    </div>

</div>