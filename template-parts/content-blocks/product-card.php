<div class="content-block card-block">
    <div class="card-title">
        <div class="container">
            <h2><?php echo get_sub_field('card_title'); ?></h2>
        </div>
    </div>
    <div class="card-products container">
        <?php
        $post_objects = get_sub_field('card_products_display');
        if ($post_objects) : ?>

            <?php foreach ($post_objects as $post) : ?>
                <?php setup_postdata($post); ?>
                <div class="card-product">
                    <div class="card-top">
                        <div class="add-to-favorites"><i class="far fa-heart"></i></div>
                        <div class="card-top__logo">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <div class="card-top__title">
                            <?php the_title(); ?>
                        </div>
                        <div class="star-top__rating star-rating">
                            <?php ar_print_star_rating(get_field('ap_rating', $post->ID)); ?>
                        </div>
                    </div>
                    <div class="card-bottom">
                        <?php if (get_field('card_main_bonus')) : ?>
                            <div class="card-bottom__bonus">
                                <?php the_field('card_main_bonus'); ?>
                            </div>
                        <?php endif; ?>
                        <?php if (get_field('card_enable_info_row')) : ?>
                            <div class="card-bottom__info">
                                <?php

                                // Check rows exists.
                                if (have_rows('card_info_row')) :

                                    // Loop through rows.
                                    while (have_rows('card_info_row')) : the_row();

                                        // Load sub field value.
                                        $feature = get_sub_field('feature');
                                        $desc = get_sub_field('description');
                                ?>

                                        <div class="line">
                                            <div class="line-left"><?php echo $feature; ?></div>
                                            <div class="line-right"><?php echo $desc; ?></div>

                                        </div>
                                <?php
                                    // End loop.
                                    endwhile;

                                endif;
                                ?>
                            </div>
                        <?php endif; ?>
                        <div class="card-bottom__play">
                            Go to and Play
                        </div>
                        <div class="card-bottom__goto">
                            Go to profile
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
            ?>
        <?php endif; ?>
    </div>

</div>