<div class="content-block which-one-block">
    <div class="container">
        <div class="which-one-text">
            <h3><?php echo get_sub_field('which_one_title'); ?></h3>
            <?php echo get_sub_field('which_one_paragraph'); ?>
        </div>

        <div class="which-one-products container">
            <?php
            $post_objects = get_sub_field('which_one_products_display');
            if ($post_objects) : ?>

                <?php foreach ($post_objects as $post) : ?>
                    <?php setup_postdata($post); ?>
                    <div class="which-one-product">
                        <div class="which-one-top">
                            <div class="which-one-img">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="which-one-title">
                                <?php the_title(); ?>
                            </div>
                            <div class="which-one-rating star-rating">
                                <?php ar_print_star_rating(get_field('ap_rating', $post->ID)); ?>
                            </div>
                        </div>
                        <div class="which-one-bottom">
                            <?php if (get_field('which_one_bonus', $post->ID)) : ?>
                                <div class="which-one-bonus">
                                    <?php the_field('which_one_bonus', $post->ID); ?>
                                </div>
                            <?php endif; ?>
                            <?php if (get_field('which_one_pluses', $post->ID) || get_field('which_one_minuses', $post->ID)) : ?>
                                <div class="which-one-features">
                                    <?php if (get_field('which_one_pluses', $post->ID)) : ?>
                                        <div class="which-one-pluses">
                                            <?php
                                            if (have_rows('which_one_pluses', $post->ID)) :
                                                while (have_rows('which_one_pluses', $post->ID)) : the_row();
                                                    $plus = get_sub_field('plus', $post->ID);
                                                    echo "<div class='which-one-plus'>" . "<span class='plus-sign'>&plus;</span>" . $plus . "</div>";
                                                endwhile;
                                            endif;
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (get_field('which_one_minuses', $post->ID)) : ?>
                                        <div class="which-one-minuses">
                                            <?php
                                            if (have_rows('which_one_minuses', $post->ID)) :
                                                while (have_rows('which_one_minuses', $post->ID)) : the_row();
                                                    $minus = get_sub_field('minus', $post->ID);
                                                    echo "<div class='which-one-minus'>" . "<span class='minus-sign'>&minus;</span>" . $minus . "</div>";
                                                endwhile;
                                            endif;
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <div class="which-one-write-review">
                                Write your review
                            </div>
                            <div class="which-one-profile">
                                Go to profile
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
                ?>
            <?php endif; ?>
        </div>

    </div>
</div>