<?php

/**
 * Footer Template
 * 
 */

?>

<?php wp_footer(); ?>
<footer id="footer" class="footer">
    <div class="container">
        <div class="footer-upper">
            <div class="footer-menus">
                <?php if (has_nav_menu("footer-menu-1")) : ?>
                    <?php echo ar_generate_footer_menu("footer-menu-1"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-2")) : ?>
                    <?php echo ar_generate_footer_menu("footer-menu-2"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-3")) : ?>
                    <?php echo ar_generate_footer_menu("footer-menu-3"); ?>
                <?php endif; ?>
                <?php if (has_nav_menu("footer-menu-4")) : ?>
                    <?php echo ar_generate_footer_menu("footer-menu-4"); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="footer-lower">
        <div class="inner-footer-lower container">
            <span class="footer-date">
                <?php echo date("Y") . " "; ?><?php _e("© All Rights Reserved", "casinon"); ?>
            </span>
            <div class="footer-images">
                <div class="footer-image-wrap">
                    <img src="<?php echo AR_DIR_URI . '/dist/img/18plus.png'; ?>" alt="">
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- .wrapper end -->
</body>

</html>