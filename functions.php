<?php

/**
 *
 * Please do not make any edits to this file. All edits should be done in a child theme.
 *
 * @package Affiliate Revolution
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

//Define theme version
define('AR_VERSION', wp_get_theme()->get('Version'));

//Define template directory
define('AR_TEMPLATE_DIR', get_template_directory());

//Define template directory uri
define('AR_DIR_URI', get_template_directory_uri());

define('DEVELOPMENT', true);

//Include theme setup files
require_once AR_TEMPLATE_DIR . '/inc/affiliate-revolution-setup.php';

//Helper functions
require_once AR_TEMPLATE_DIR . '/inc/affiliate-product-cpt.php';

//General code for theme - scripts, styles
require_once AR_TEMPLATE_DIR . '/inc/affiliate-revolution-enqueue.php';

//Helper functions
require_once AR_TEMPLATE_DIR . '/inc/affiliate-revolution-helpers.php';

//Include ACF setup files
require_once AR_TEMPLATE_DIR . '/inc/affiliate-revolution-acf-setup.php';

//Include ACF fields for theme
require_once AR_TEMPLATE_DIR . '/inc/affiliate-revolution-acf-fields.php';
