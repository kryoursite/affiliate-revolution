<?php

/**
 * The site's entry point.
 *
 * @package Affiliate Revolution
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();

if (is_front_page()) {
    get_template_part('template/front-page');
} elseif (is_single()) {
    get_template_part('template/affiliate-product');
} elseif (is_author()) {
    //If is casino load casino template
    get_template_part('template/author');
} elseif (is_archive()) {
    //If is archive load archive template
    get_template_part('template/archive');
} elseif (is_search()) {
    //If is search load search template
    get_template_part('template/search');
} else {
    get_template_part('template/404');
}

get_footer();
